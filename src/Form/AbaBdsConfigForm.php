<?php

namespace Drupal\aba_bds\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines configuration form for BDS API settings.
 */
class AbaBdsConfigForm extends ConfigFormBase {

  /**
   * BDS API settings machine name.
   *
   * @var string
   */
  const SETTINGS = 'aba_bds.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aba_bds_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#default_value' => $config->get('username'),
    ];

    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#description' => $this->t('Leave this field blank to keep password unchanged.'),
    ];

    $form['url'] = [
      '#type' => 'url',
      '#title' => $this->t('Url'),
      '#default_value' => $config->get('url'),
      '#description' => $this->t('Use the format protocol://domain. For example: https://bds9.booksense.com'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // If the password field is left blank, keep the current value.
    $password = $form_state->getValue('password');
    if (empty($password)) {
      $password = $this->config(static::SETTINGS)->get('password');
    }

    // Actually save the settings.
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('username', trim($form_state->getValue('username')))
      ->set('password', trim($password))
      ->set('url', $form_state->getValue('url'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
