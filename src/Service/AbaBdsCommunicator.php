<?php

namespace Drupal\aba_bds\Service;

use AbaBds\AbaBdsApiException;
use AbaBds\AbaBdsBook;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\file\FileInterface;
use Drupal\file\FileRepositoryInterface;
use GuzzleHttp\Client;
use Nicebooks\Isbn\IsbnTools;

/**
 * Class to define interactions between BDS and Drupal.
 */
class AbaBdsCommunicator {

  const ALLOWED_LANGUAGES = [
    'English',
  ];

  /**
   * Cache backend service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Logger factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * ISBN tools.
   *
   * @var \Nicebooks\Isbn\IsbnTools
   */
  protected $isbnTools;

  /**
   * The BDS API library.
   *
   * @var \AbaBds\AbaBdsBook
   */
  protected $bdsApi;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * The file respository service.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected $fileRepository;

  /**
   * The service constructor.
   *
   * @param Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   * @param Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend service.
   * @param Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param GuzzleHttp\Client $client
   *   The HTTP client.
   * @param \Drupal\file\FileRepositoryInterface $file_repository
   *   The file repository service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    LoggerChannelFactoryInterface $logger_factory,
    CacheBackendInterface $cache_backend,
    EntityTypeManagerInterface $entity_type_manager,
    FileSystemInterface $file_system,
    Client $client,
    FileRepositoryInterface $file_repository
  ) {
    $this->configFactory = $config_factory;
    $this->loggerFactory = $logger_factory;
    $this->cacheBackend = $cache_backend;
    $this->entityTypeManager = $entity_type_manager;
    $this->fileSystem = $file_system;
    $this->client = $client;
    $this->fileRepository = $file_repository;
    $this->isbnTools = new IsbnTools();
    $this->initBdsApiLibrary();
  }

  /**
   * Performs a search against BDS API.
   *
   * @param string $criteria
   *   The search criteria.
   * @param int $page
   *   The number of results page to retrieve.
   * @param int $items_per_page
   *   Amount of items to be returned per page.
   *
   * @return array
   *   List of search results.
   */
  public function search(string $criteria, int $page = 1, int $items_per_page = 25): array {
    $results = [];

    if ($this->bdsApi) {
      try {
        $response = $this->bdsApi->search($criteria, $page, $items_per_page);
        $results = $response->results;
      }
      catch (AbaBdsApiException $e) {
        $message = $e->getMessage() ?: $e->getPrevious()->getMessage() ?? '';
        $this->loggerFactory->get('aba_bds')->error('An error occurred when performing a search against BDS. "{message}"', [
          'message' => $message,
        ]);
      }
    }

    return $results;
  }

  /**
   * Retrieves information about a book from the BDS, based on ISBN.
   *
   * @param string $isbn
   *   The ISBN (13 digit) of the book to search for.
   *
   * @return object|null
   *   The book information, or NULL.
   */
  public function getBookByIsbn(string $isbn = ''): ?object {
    $book = NULL;

    if ($this->bdsApi) {
      if ($this->isbnTools->isValidIsbn($isbn)) {
        try {
          $response = $this->bdsApi->getBook($isbn);
          $book = $response->{$isbn} ?? NULL;
        }
        catch (AbaBdsApiException $e) {
          $message = $e->getMessage() ?: $e->getPrevious()->getMessage() ?? '';
          $this->loggerFactory->get('aba_bds')->error('An error occurred requesting book information from the BDS. "{message}"', [
            'message' => $message,
          ]);
        }
      }
    }

    return $book;
  }

  /**
   * Retrieves information about a book from the BDS, based on ISBN.
   *
   * @param array $isbns
   *   An array of ISBNs (13 digit) to retrieve.
   *
   * @return object|null
   *   The book information, or NULL.
   */
  public function getMultipleBooksByIsbns(array $isbns = []): ?object {
    $response = NULL;

    if ($this->bdsApi) {
      $isbns = implode(',', $isbns);
      try {
        $response = $this->bdsApi->getBook($isbns);
      }
      catch (AbaBdsApiException $e) {
        $message = $e->getMessage() ?: $e->getPrevious()->getMessage() ?? '';
        $this->loggerFactory->get('aba_bds')->error('An error occurred requesting book information from the BDS. "{message}"', [
          'message' => $message,
        ]);
      }
    }

    return $response;
  }

  /**
   * Add a new book to the BDS.
   *
   * @param string $isbn
   *   The ISBN.
   * @param array $book_params
   *   The book metadata.
   *
   * @return object|null
   *   The book information, or NULL.
   */
  public function addBook($isbn, $book_params) {
    $response = NULL;

    if ($this->bdsApi) {
      try {
        $response = $this->bdsApi->addBook($isbn, $book_params);
      }
      catch (AbaBdsApiException $e) {
        $message = $e->getMessage() ?: $e->getPrevious()->getMessage() ?? '';
        $response = new \stdClass();
        $response->message = $message;
        $this->loggerFactory->get('aba_bds')->error('An error occurred adding this book to the BDS. "{message}"', [
          'message' => $message,
        ]);
      }
    }

    return $response;
  }

  /**
   * Update an existing book in the BDS.
   *
   * @param string $isbn
   *   The book's ISBN.
   * @param array $book_params
   *   The metadata to update.
   *
   * @return object|null
   *   The book information, or NULL.
   */
  public function updateBook($isbn, $book_params) {
    $response = NULL;

    if ($this->bdsApi) {
      try {
        $response = $this->bdsApi->updateBook($isbn, $book_params);
      }
      catch (AbaBdsApiException $e) {
        $message = $e->getMessage() ?: $e->getPrevious()->getMessage() ?? '';
        $this->loggerFactory->get('aba_bds')->error('An error occurred updating this book in the BDS. "{message}"', [
          'message' => $message,
        ]);
      }
    }

    return $response;
  }

  /**
   * Retrieves information about books from the BDS, based on BISAC codes.
   *
   * @param array $bisac_codes
   *   List of bisac codes to search books.
   * @param int $page
   *   The number of results page to retrieve.
   * @param int $amount
   *   Amount of books to retrieve from the BDS.
   *
   * @return array
   *   Array of book information objects from the BDS.
   */
  public function getBooksByBisacCodes(array $bisac_codes = [], int $page = 1, int $amount = 10): array {
    $books = [];

    if ($this->bdsApi) {
      try {
        $response = $this->bdsApi->searchByBisacCode($bisac_codes, $page, $amount);
        $books = $response->results ?? [];
      }
      catch (AbaBdsApiException $e) {
        $message = $e->getMessage() ?: $e->getPrevious()->getMessage() ?? '';
        $this->loggerFactory->get('aba_bds')->error('An error occurred while searching books by BISAC codes. "{message}"', [
          'message' => $message,
        ]);
      }
    }

    return $books;
  }

  /**
   * Retrieve information about all root BISAC codes from the BDS API.
   *
   * @return array
   *   List of root BISAC codes, keyed by code.
   */
  public function getRootBisacCodes(): array {
    $codes = [];

    if ($this->bdsApi) {
      try {
        $response = $this->bdsApi->getRootBisacCodes();
        $codes = $response->root->children;
      }
      catch (AbaBdsApiException $e) {
        $message = $e->getMessage() ?: $e->getPrevious()->getMessage() ?? '';
        $this->loggerFactory->get('aba_bds')->error('An error occurred when retrieving root BISAC codes from the BDS. "{message}"', [
          'message' => $message,
        ]);
      }
    }

    return $codes;
  }

  /**
   * Return the BDS API object for manual queries.
   *
   * @return \AbaBds\AbaBdsBook
   */
  public function getBdsApi() {
    return $this->bdsApi;
  }

  /**
   * Process a date to a format that can be handled by Drupal.
   *
   * @param string $date
   *   The date to be processed.
   *
   * @return Drupal\Core\Datetime\DrupalDateTime|null
   *   A datetime object with the received date, or NULL.
   */
  protected function getDate(string $date = ''): ?DrupalDateTime {
    $processed_date = NULL;
    try {
      $processed_date = DrupalDateTime::createFromFormat('U', empty($date) ? time() : strtotime($date));
    }
    catch (\Exception $e) {
      // Do nothing.
    }
    return $processed_date;
  }

  /**
   * Initialize BDS API library.
   */
  protected function initBdsApiLibrary() {
    $config = $this->configFactory->get('aba_bds.settings');
    $this->bdsApi = new AbaBdsBook(
      $config->get('username'),
      $config->get('password'),
      $config->get('url')
    );

    // Check if a previously generated auth token exists.
    $token = $this->cacheBackend->get('bdsapi:token');
    if ($token) {
      // If there is a cached token, use it.
      $this->bdsApi->setToken($token ? $token->data : NULL);
    }
    else {
      // No cached token, so generate a new one and cache it.
      try {
        $token = $this->bdsApi->getJsonWebToken();
        $this->cacheBackend->set('bdsapi:token', $token, $token->expires_at);
        $this->bdsApi->setToken($token);
      }
      catch (AbaBdsApiException $e) {
        // Do nothing.
      }
    }
  }

}
